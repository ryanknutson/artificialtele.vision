//import './style.css'
import javascriptLogo from './javascript.svg'
import viteLogo from '/vite.svg'

import * as THREE from 'three'
import { GLTFLoader } from 'three/examples/jsm/loaders/GLTFLoader.js'
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls'

// enable or disable helpers
const dev = false

// tv model
let tv

// flash "click for music" text
let flashMusicText = true

const scene = new THREE.Scene()

const camera = new THREE.PerspectiveCamera(75, window.innerWidth / window.innerHeight, 0.6, 1200)
camera.position.z = 5
// camera.position.x = 5




const renderer = new THREE.WebGLRenderer({antialias: true})

// scene background color
// renderer.setClearColor("#000000")
// renderer.setClearColor("#192BC2")
renderer.setClearColor("#0000FF")

// set id of canvas
renderer.domElement.id = 'canvas'

// set size
renderer.setSize(window.innerWidth, window.innerHeight)
document.body.appendChild(renderer.domElement)


const controls = new OrbitControls(camera, renderer.domElement)



// create an AudioListener and add it to the camera
const listener = new THREE.AudioListener();
camera.add( listener );

// create a global audio source
const sound = new THREE.Audio( listener );

// load a sound and set it as the Audio object's buffer
const audioLoader = new THREE.AudioLoader();
audioLoader.load( '/funkytown.mp3', function( buffer ) {
	sound.setBuffer( buffer );
	sound.setLoop( true );
	sound.setVolume( 0.5 );

  function soundToggle() {
    if (sound.isPlaying) {
      sound.pause(); // Pause the audio if it's already playing
    } else {
      sound.play();
    }
  }

  if (!dev) {
    document.onpointerdown = function(e) {
      switch (e.button) {
        // left mouse
        case 0: sound.play()
            flashMusicText = false 
            isVisible(musicText, false)
            break;
        // right mouse
        case 2: soundToggle()
            flashMusicText = false 
            isVisible(musicText, false)
      }
    }
  }
})


// update on resize
window.addEventListener('resize', () => {
  renderer.setSize(window.innerWidth, window.innerHeight);
  camera.aspect = window.innerWidth / window.innerHeight;
  camera.updateProjectionMatrix();
})

// load the tv model in
const loader = new GLTFLoader()
loader.load('/gltf/Fony3.glb',
  (gltf) => {
    tv = gltf.scene;
    scene.add(tv);
    tv.scale.set(4.5, 4.5, 4.5)
    tv.position.set(0,-.7,0)
    tv.rotation.z += 0.16
    // tv.rotation.y += 0.8
  },
  (xhr) => {
    // Progress callback (optional)
    console.log((xhr.loaded / xhr.total) * 100 + '% loaded');
  },
  (error) => {
    console.error('Error loading GLB model:', error);
  }
)

let musicText


const clock = new THREE.Clock()
clock.start()
const soundTextLoader = new GLTFLoader()
soundTextLoader.load(
	// resource URL
	'/gltf/clickformusic.glb',
	// called when the resource is loaded
  (gltf) => {
    musicText = gltf.scene;
    scene.add(musicText);
    musicText.scale.set(4.5, 4.5, 4.5)
    musicText.position.set(0,-2.5,0)
  },
  (xhr) => {
    // Progress callback (optional)
    console.log((xhr.loaded / xhr.total) * 100 + '% loaded');
  },
  (error) => {
    console.error('Error loading GLB model:', error);
  }
)

// front light
const directionalLight = new THREE.DirectionalLight(0xffffff, 1);
directionalLight.position.set(0, 1, 5);
scene.add(directionalLight);

// back light
const backLight = new THREE.DirectionalLight(0xffffff, 1);
backLight.position.set(0, 0, -5);
scene.add(backLight);

if (dev) {
  // front light helper
  const helper = new THREE.DirectionalLightHelper(directionalLight, 2);
  scene.add( helper );
  
  // back light helper
  const backLightHelper = new THREE.DirectionalLightHelper(backLight, 2);
  scene.add(backLightHelper);
  
  // show grid
  const size = 10;
  const divisions = 10;
  const gridHelper = new THREE.GridHelper( size, divisions );
  scene.add(gridHelper);
}

function isVisible(object, visible = false) {
  object.traverse(function (child) {
    if (child instanceof THREE.Mesh) {
      child.visible = visible
    }
  });
}

// render
const rendering = function() {

  requestAnimationFrame(rendering)
  
  // rotate the tv
  if (tv) tv.rotation.y += 0.007

  // console.log(clock.getElapsedTime()) 

  let sec = clock.getElapsedTime()
  console.log(Math.floor(sec))


  
  // get the digit of ms in current second
  let ms = Math.round(10*(Math.abs(sec % 1))) 
  // console.log(ms)
  if (dev) isVisible(musicText, true)

  if (Math.floor(sec) == 5) {
    flashMusicText = false 
    isVisible(musicText, false)
  }

  if (flashMusicText) {
    if (ms == 0) isVisible(musicText, true)
    else if (ms == 5) isVisible(musicText, false)
  }


  // user controls
  controls.update()

  // render it!
  renderer.render(scene, camera)
}

rendering()
